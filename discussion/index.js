console.log('Waddup waddup?')

//Create a function that will count to a series of number depending on the input of the user.

//Get the value of the input field. In order to get the value of the input, using a dot(.) notation, call out its value property.

//There are 5 ways to select elements (5 DOM Selectors)
	//1. getElementbyTagName() - collective through the use of the element/tag name.
	//2. getElementByClassName() - collective through the use of their class attributes.
	//3. getElementById() - select a distinct/specific through the use of its ID attribute.
	//4. querySelector() - neutral/versatile selection
	//5. querySelectorAll() - collective approach when selecting multiple components/elements at once.


function countUsingWhile() {
	let input1 = document.getElementById('task1').value;
	if (input1 <= 0) {
		//inform the user that the input is not valid.
		//innerHTML Property.
		let msg = document.getElementById('message');
		msg.innerHTML = 'Invalid value';
	} else {
		while (input1 !== 0) {
		//what will happen if the condition has not met.
		alert(input1);
		input1-- //decrease the value of the input by 1 per iteration of the loop.
		}
	}
	// alert(input1);
}


// Create a function that will count to a series of number depending on the value inserted by the user.

function countUsingDoWhile() {
	//Get the input of the user.
	let input2 = document.getElementById('task2').value;
	// Make sure that the value is valid(we won't be accepting any values that are <= 0)
	if (input2 <= 0) {
		let mensahe = document.getElementById('info');
		mensahe.innerHTML = 'You are not allowed to proceed';
	} else {
		// using DOM selectors we will target the container element.
			let indexStart = 1
		do {
			let text = document.getElementById('info');
			text.innerHTML = input2 + ' is valid.';
			indexStart++
		} while (indexStart <= input2);
	}
}

//[SECTION] For loop
//Syntax: for(initialization; expression/condition; finalExpression/iteration) {}

//Task: Count to a series number depending on the value inserted by the user.
function countUsingForLoop() {
	let loop = document.getElementById('task3').value;
	let luffy = document.getElementById('response');
	if (loop <= 0) {
		luffy.innerHTML = 'Nope. No. Na-uh.';
	} else {
		for (let startCount = 0; startCount <= loop; startCount++) {
			//For every iteration, should add a value of 1 to eventually meet the condition and terminate the process.
			//Describe what will happen per iteration.
			alert(startCount);
		}
	}
}
//How to access elements of a string using repetition control structures.

//Get the input of the user.
//Analyze the value that will be inserted by the user.

function getElementsString() {
	let name = document.getElementById('userName').value;
	let textLength = document.getElementById('stringLength');
	// alert(typeof name);

	//Validate and make sure that the input is not equivalent to blank.
	if (name !== '') {
		//response if truthy
		//Analyze the value that was inserted by the user.
			//1. Determine the length of the string.
				//Invoke the .length of a string.
		textLength.innerHTML = 'The string is ' + name.length + ' characters long';

		//Upon accessing elements inside a string, this can be done so using [] square brackets.
		//We can access each element through the use of its index number/count.
		//The count will start from 0. First character inside the string corresponds to the number 0.
		// console.log(name[0]);
		// console.log(name[1]);
		// console.log(name[2]);
		// console.log(name[3]);
		// console.log(name[4]);
		// console.log(name[5]);
		// console.log(name[6]);
		// console.log(name[7]);
	//Use the concept of loops in order to produce a much more flexible response for the users input.
		for (let startName = 0; startName < name.length; startName++) {
			//Access each element
			console.log(name[startName]);
		}
	} else {
		//response if falsy
		alert('Try again');
	}
}